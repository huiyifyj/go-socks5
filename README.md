# socks5

> Provides the `socks5` package that implements a [SOCKS5 server](http://en.wikipedia.org/wiki/SOCKS).

![gitlab-ci](https://img.shields.io/gitlab/pipeline/huiyifyj/go-socks5/master?logo=gitlab&style=flat-square)

SOCKS (Secure Sockets) is used to route traffic between a client and server through
an intermediate proxy layer. This can be used to bypass firewalls or NATs.

## Feature

The package has the following features:

- "No Auth" mode
- User/Password authentication
- Support for the CONNECT command
- Rules to do granular filtering of commands
- Custom DNS resolution
- Unit tests

## Example

Below is a simple example of usage

```go
conf := &socks5.Config{}
server, err := socks5.New(conf) // New and create a SOCKS5 server
if err != nil {
    panic(err)
}
// Create SOCKS5 proxy on localhost port 8000
if err := server.ListenAndServe("tcp", "127.0.0.1:8000"); err != nil {
    panic(err)
}
```
